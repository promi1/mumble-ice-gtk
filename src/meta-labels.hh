/*
    mumble-ice-gtk - GUI Virtual server manager for Murmur
    Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include "Murmur.h"

class MetaLabels {
private:
  Murmur::MetaPrx &meta;
  Gtk::Label version_label;
  Gtk::Label uptime_label;
  void add_version_label (Gtk::Box &box);
  void add_uptime_label (Gtk::Box &box);
public:
  MetaLabels (Murmur::MetaPrx &meta);
  void add_labels (Gtk::Box &box);
};
