/*
  mumble-ice-gtk - GUI Virtual server manager for Murmur
  Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <gtkmm/application.h>
#include <Ice/Ice.h>
#include <iostream>
#include <stdexcept>

#include "main-adapter.hh"

constexpr auto appid = "com.gitlab.promi1.mumble-ice-gtk";

int
real_main(int argc, char* argv[]) {
  Ice::CommunicatorHolder ich (argc, argv);
  auto base = ich->stringToProxy("Meta:default -p 6502");
  auto meta = Ice::checkedCast<Murmur::MetaPrx>(base);
  if (!meta) {
    throw std::runtime_error("Invalid proxy");
  }

  auto app = Gtk::Application::create(argc, argv, appid);
  Gtk::Window window;
  MainAdapter adapter (window, *meta);
  window.show_all ();
  return app->run(window);
}

int
main(int argc, char* argv[]) {
  try {
    return real_main (argc, argv);
  }
  catch(const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
}
