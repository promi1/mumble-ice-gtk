/*
    mumble-ice-gtk - GUI Virtual server manager for Murmur
    Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <glibmm/i18n.h>
#include <iomanip>

#include "uptime.hh"

std::string
uptime_to_string (int total_secs) {
  constexpr int secs_per_min = 60;
  constexpr int secs_per_hour = 60 * 60;
  constexpr int secs_per_day = 24 * 60 * 60;

  auto days = total_secs / secs_per_day;
  auto hours = (total_secs % secs_per_day) / secs_per_hour;
  auto mins = (total_secs % secs_per_hour) / secs_per_min;
  auto secs = total_secs % secs_per_min;
  
  std::stringstream ss;

  ss << days << " " << _("days") << " ";
  ss << std::setfill('0') << std::setw(2) << hours << ":";
  ss << std::setfill('0') << std::setw(2) << mins << ":";
  ss << std::setfill('0') << std::setw(2) << secs;

  return ss.str ();
}
