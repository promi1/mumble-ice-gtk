/*
    mumble-ice-gtk - GUI Virtual server manager for Murmur
    Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <memory>
#include <glibmm/ustring.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/treeview.h>
#include <sigc++/sigc++.h>

#include "Murmur.h"

class ServersTreeView {
private:
  Murmur::MetaPrx &meta;

  Gtk::Box buttons_box;
  Gtk::Button add_button;
  Gtk::Button start_button;
  Gtk::Button stop_button;
  Gtk::Button delete_button;

  Gtk::TreeModel::ColumnRecord columns;
  Gtk::TreeModelColumn<int> id_column;
  Gtk::TreeModelColumn<bool> running_column;
  Gtk::TreeModelColumn<Glib::ustring> name_column;

  Gtk::ScrolledWindow scrolled_window;
  Gtk::TreeView tree_view;
  Glib::RefPtr<Gtk::ListStore> list_store;
  Glib::RefPtr<Gtk::TreeSelection> tree_selection;

  sigc::signal<void, std::shared_ptr<Murmur::ServerPrx>> changed_signal;
  
  void add ();
  void start ();
  void stop ();
  void delete_ ();
  void refresh (int selected);
  std::pair<int, std::shared_ptr<Murmur::ServerPrx>> get_selected_server ();
  void selection_changed ();
  using SignalHandler = decltype (&ServersTreeView::add);
  void set_button (Gtk::Button &button, std::string label,
		   SignalHandler signal_handler);
  std::optional<std::string> get_server_register_name (Murmur::ServerPrx &server);
  std::optional<std::string> get_default_register_name ();
  std::string get_root_channel_name (Murmur::ServerPrx &server);
  std::string get_name (Murmur::ServerPrx &server);
  void add_to_list_store (Murmur::ServerPrx &server, int selected);
public:
  ServersTreeView (Murmur::MetaPrx &meta);
  void add_buttons (Gtk::Box &box);
  void add_tree (Gtk::Box &box);
  sigc::signal<void, std::shared_ptr<Murmur::ServerPrx>> signal_changed ();
};
