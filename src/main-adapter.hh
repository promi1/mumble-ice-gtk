/*
    mumble-ice-gtk - GUI Virtual server manager for Murmur
    Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <memory>
#include <gtkmm/box.h>
#include <gtkmm/window.h>

#include "meta-labels.hh"
#include "Murmur.h"
#include "server-conf-tree.hh"
#include "servers-tree-view.hh"

class MainAdapter {
private:
  Gtk::Box outer_box;
  Gtk::Box middle_box;
  Gtk::Box right_box;

  MetaLabels meta_labels;
  ServersTreeView servers_tree_view;
  ServerConfTree server_conf_tree;

  void servers_tree_view_changed (std::shared_ptr<Murmur::ServerPrx> server);
public:
  MainAdapter (Gtk::Window &window, Murmur::MetaPrx &meta);
};
