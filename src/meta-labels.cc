/*
  mumble-ice-gtk - GUI Virtual server manager for Murmur
  Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <glibmm/i18n.h>

#include "meta-labels.hh"
#include "uptime.hh"

MetaLabels::MetaLabels (Murmur::MetaPrx &meta) : meta (meta) {
  
}

void
MetaLabels::add_version_label (Gtk::Box &box) {
  int major;
  int minor;
  int patch;
  std::string text;

  meta.getVersion (major, minor, patch, text);

  std::stringstream version_ss;
  version_ss << _("murmur version: ") << major << "." << minor << "."
	     << patch << " (" << text << ")";
  version_label.set_text (version_ss.str ());
  box.pack_start (version_label, false, true);
}

void
MetaLabels::add_uptime_label (Gtk::Box &box) {
  auto uptime = meta.getUptime ();

  std::stringstream uptime_ss;
  uptime_ss << _("uptime: ") << uptime_to_string (uptime);
    
  uptime_label.set_text (uptime_ss.str ());
  box.pack_start (uptime_label, false, true);
}

void
MetaLabels::add_labels (Gtk::Box &box) {
  add_version_label (box);
  add_uptime_label (box);
}
