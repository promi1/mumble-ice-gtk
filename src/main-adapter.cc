/*
  mumble-ice-gtk - GUI Virtual server manager for Murmur
  Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "main-adapter.hh"

MainAdapter::MainAdapter (Gtk::Window &window, Murmur::MetaPrx &meta)
  : meta_labels (meta), servers_tree_view (meta), server_conf_tree (meta) {
  window.set_default_size(1000, 800);

  middle_box.set_orientation (Gtk::ORIENTATION_VERTICAL);
  middle_box.set_size_request (400, -1);
  outer_box.pack_start (middle_box, false, true);

  right_box.set_orientation (Gtk::ORIENTATION_VERTICAL);
  outer_box.pack_start (right_box, true, true);

  meta_labels.add_labels (middle_box);

  servers_tree_view.add_buttons (middle_box);
  servers_tree_view.add_tree (middle_box);
    
  server_conf_tree.add_server_conf_tree (right_box);
  // add_log_tree (right_box);

  window.add (outer_box);

  servers_tree_view.signal_changed ()
    .connect (sigc::mem_fun (*this, &MainAdapter::servers_tree_view_changed));
}

void
MainAdapter::servers_tree_view_changed (std::shared_ptr<Murmur::ServerPrx> server) {
  server_conf_tree.set_server (server);
}
