/*
    mumble-ice-gtk - GUI Virtual server manager for Murmur
    Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <glibmm/i18n.h>
#include <servers-tree-view.hh>

ServersTreeView::ServersTreeView (Murmur::MetaPrx &meta) : meta (meta) {
  columns.add (id_column);
  columns.add (running_column);
  columns.add (name_column);
}

void
ServersTreeView::add () {
  auto server = meta.newServer ();
  refresh (server->id ());
}

std::pair<int, std::shared_ptr<Murmur::ServerPrx>>
ServersTreeView::get_selected_server () {
  auto iter = tree_selection->get_selected ();
  if (!iter) {
    return std::make_pair (-1, nullptr);
  }
  Gtk::TreeModel::Row row = *iter;

  auto server_id = row[id_column];
  std::shared_ptr<Murmur::ServerPrx> server = meta.getServer (server_id);
  return std::make_pair (server_id, server);
}
  
void
ServersTreeView::start () {
  auto [server_id, server] = get_selected_server ();
  if (!server) {
    return;
  }
  server->start ();
  refresh (server_id);
}

void
ServersTreeView::stop () {
  auto [server_id, server] = get_selected_server ();
  if (!server) {
    return;
  }
  server->stop ();
  refresh (server_id);
}

void
ServersTreeView::delete_ () {
  auto [server_id, server] = get_selected_server ();
  if (!server) {
    return;
  }
  // A running server can not be deleted
  if (server->isRunning ()) {
    return;
  }
  server->_cpp_delete ();
  refresh (-1);
}

void
ServersTreeView::set_button (Gtk::Button &button, std::string label,
			     SignalHandler signal_handler) {
  button.set_label (label);
  button.signal_clicked ().connect (sigc::mem_fun (*this, signal_handler));
  buttons_box.add (button);
}

void
ServersTreeView::add_buttons (Gtk::Box &box) {
  set_button (add_button, _("new"), &ServersTreeView::add);
  set_button (start_button, _("start"), &ServersTreeView::start);
  set_button (stop_button, _("stop"), &ServersTreeView::stop);
  set_button (delete_button, _("delete"), &ServersTreeView::delete_);
  box.add (buttons_box);
}

void
ServersTreeView::add_tree (Gtk::Box &box) {
  list_store = Gtk::ListStore::create (columns);

  refresh (-1);

  tree_view.set_model (list_store);
  tree_view.append_column (_("id"), id_column);
  tree_view.append_column (_("is running"), running_column);
  tree_view.append_column (_("name"), name_column);
  tree_selection = tree_view.get_selection ();

  tree_selection->signal_changed()
    .connect(sigc::mem_fun(*this, &ServersTreeView::selection_changed));
 
  scrolled_window.add (tree_view);
  box.pack_start (scrolled_window, true, true);
}

std::optional<std::string>
ServersTreeView::get_server_register_name (Murmur::ServerPrx &server) {
  std::string registername;
  registername = server.getConf ("registername");
  if (registername.empty ()) {
    return std::nullopt;
  }
  else {
    return registername;
  }
}

std::optional<std::string>
ServersTreeView::get_default_register_name () {
  auto const default_conf = meta.getDefaultConf ();
  auto it = default_conf.find ("registername");
  if (it == std::end (default_conf)) {
    return std::nullopt;
  }
  else {
    return it->second;
  }
}

std::string
ServersTreeView::get_root_channel_name (Murmur::ServerPrx &server) {
  auto channel = server.getChannelState (0);
  return channel.name;
}

std::string
ServersTreeView::get_name (Murmur::ServerPrx &server) {
  auto register_name = get_server_register_name (server);
  if (register_name) {
    return *register_name;
  }
  register_name = get_default_register_name ();
  if (register_name) {
    return *register_name;
  }
  return get_root_channel_name (server);
}

void
ServersTreeView::add_to_list_store (Murmur::ServerPrx &server,
					   int selected) {
  auto iter = list_store->append ();
  Gtk::TreeModel::Row row = *iter;

  auto id = server.id ();
  row[id_column] = id;
  row[running_column] = server.isRunning ();
  row[name_column] = get_name (server);
  if (id == selected) {
    tree_selection->select (iter);
  }
}

void
ServersTreeView::refresh (int selected) {
  list_store->clear ();
  auto servers = meta.getAllServers ();
  for (const auto &server : servers) {
    add_to_list_store (*server, selected);
  }
}

void
ServersTreeView::selection_changed () {
  auto [server_id, server] = get_selected_server ();
  changed_signal.emit (server);
}

sigc::signal<void, std::shared_ptr<Murmur::ServerPrx>>
ServersTreeView::signal_changed () {
  return changed_signal;
}
