/*
    mumble-ice-gtk - GUI Virtual server manager for Murmur
    Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <glibmm/refptr.h>
#include <gtkmm/box.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/treeview.h>
#include <memory>

#include "Murmur.h"

class ServerConfTree {
private:
  Murmur::MetaPrx &meta;
  
  Gtk::TreeModel::ColumnRecord columns;
  Gtk::TreeModelColumn<Glib::ustring> key_column;
  Gtk::TreeModelColumn<Glib::ustring> value_column;

  Gtk::ScrolledWindow scrolled_window;
  Gtk::TreeView tree_view;
  Glib::RefPtr<Gtk::ListStore> list_store;
  std::string get_item_value (const std::map<std::string, std::string> &server_conf,
			      std::string key, std::string default_value);
  void add_item_to_list_store (std::string key, std::string value);
public:
  ServerConfTree (Murmur::MetaPrx &meta);
  void add_server_conf_tree (Gtk::Box &box);
  void set_server (std::shared_ptr<Murmur::ServerPrx> server);
};
