/*
  mumble-ice-gtk - GUI Virtual server manager for Murmur
  Copyright (C) 2018  Phobos <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <glibmm/i18n.h>
#include "server-conf-tree.hh"

ServerConfTree::ServerConfTree (Murmur::MetaPrx &meta) : meta (meta) {

}

void
ServerConfTree::add_server_conf_tree (Gtk::Box &box) {
  columns.add (key_column);
  columns.add (value_column);

  list_store = Gtk::ListStore::create (columns);

  tree_view.set_model (list_store);
  tree_view.append_column (_("key"), key_column);
  tree_view.append_column (_("value"), value_column);

  scrolled_window.add (tree_view);
  box.pack_start (scrolled_window, true, true);
}

bool
iequals(std::string const &a, std::string const &b) {
  return std::equal(a.begin(), a.end(),
		    b.begin(), 
		    [](char a, char b) {
		      return tolower(a) == tolower(b);
		    });
}

std::string
ServerConfTree::get_item_value (std::map<std::string, std::string> const &server_conf,
				std::string key, std::string default_value) {
  if (iequals (key, "registerpassword")) {
    return "******";
  }

  auto const it = server_conf.find (key);
  if (it != std::cend (server_conf)) {
    return it->second;
  }

  return default_value;
}

void
ServerConfTree::add_item_to_list_store (std::string key, std::string value) {
  auto iter = list_store->append ();
  Gtk::TreeModel::Row row = *iter;
  row[key_column] = key;
  row[value_column] = value;
}

void
ServerConfTree::set_server (std::shared_ptr<Murmur::ServerPrx> server) {
  list_store->clear ();
  if (!server) {
    return;
  }

  auto const default_conf = meta.getDefaultConf ();
  auto const server_conf = server->getAllConf ();

  for (auto const &item : default_conf) {
    add_item_to_list_store (item.first,
			    get_item_value (server_conf, item.first, item.second));
  }
}
